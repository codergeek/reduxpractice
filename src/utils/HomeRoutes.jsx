import React from 'react'
import {Navigate, Outlet} from 'react-router-dom'

const useAuth=()=>{
    const userInfo = JSON.parse(localStorage.getItem("user-info"));
    if(userInfo?.name != undefined){
      return true    
    } else {
      return false
    }
  }
const HomeRoutes = () => {

    const auth=useAuth()

    return auth?<Outlet/>: <Navigate to="/login"/>
}

export default HomeRoutes