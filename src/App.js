import logo from "./logo.svg";
import "./App.css";
import {
  Navigate,
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import HomeRoutes from "./utils/HomeRoutes";
import Layout from "./utils/Layout";
import Home from "./components/Home";
import Profile from "./components/Profile";
import PublicRoutes from "./utils/PublicRoutes";
import Login from "./components/Login";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<HomeRoutes/>}>
        <Route path="/" element={<Layout/>}>
          <Route path="/" element={<Navigate replace to="/home"/>}/>
          <Route path="/home" element={<Home/>}/>
          <Route path="/profile" element={<Profile/>}/>

        </Route>
        </Route>
        <Route path="/" element={<PublicRoutes/>}>
         <Route path="/login" element={<Login/>}/>
        </Route>
      </Routes>
    </>
  );
}

export default App;
